# sustainable-buildings-master

Recursos para aprender sobre edificación sostensible.

## Consideraciones
- Todos los los recursos visualizables y descargables en este repositorio son de libre y gratuito acceso.
- Se encuentran enlistados algunos recursos de paga con sus links oficiales, procurando el proveedor más económico posible.
- Este repositorio se trabajará primero en español con fines de accesibilidad para hispanohablantes. No obstante, la mayoría de la información citada se encuentra en inglés, por lo cual se recomienda de sobremanera un buen dominio del idioma.

## Sistemas de evaluación
[LEED](#LEED)
  - BD+c
  - O+M
  - ND
- WELL
- EDGE
- BOMA

# LEED
Recurso principal:
https://www.usgbc.org/credits

